﻿var pageNoVal = 1, SNoVal = 1, api;
var summaryTrendUrl = "/api/summaryDefectTrend"
var DefectSummary = [];
var dateParam = null;
var labelArray = ["Tables", "Rows", "Data"]
var dates = [];
var jsonResult;
var modal = null, modal2 = null; //pop-up modal
var FcurrentPage = 1;//current page in audit run inspection
var PcurrentPage = 1;//current page in audit run inspection
var audit = "Failed - Has Defects";
var failSNo = 1;//serial number strat from 1 in audit run inspection
var passSNo = 1;//serial number strat from 1 in audit run inspection
var NewpageNo;//switch new page fromm audit run inspection table
var res;
var statsHTML;
var PURPOSE_1 = "DASHBOARD_TREND";
var PURPOSE_2 = "DETAILS_TREND";
showCharts(dateParam);
var detailPageLength = 10;

$(function () {
    $("#datepicker-1").datepicker();
    $("#datepicker-1").on("change", function () {
        dates = new String($(this).val());       
        res = dates.split("/");
        dateParam = res[1] + "/" + res[0] + "/" + res[2];      
        showCharts(dateParam);
        FcurrentPage = failSNo =passSNo= 1;
        fetchData("Failed - Has Defects", FcurrentPage, failSNo);
        fetchData("success", PcurrentPage, passSNo);
    });
});


var actualDate;
function showCharts(dateParam) {
    summaryTrendUrl = "/api/summaryDefectTrend";
    $.ajax({
        url: summaryTrendUrl,
        data: { date: dateParam, DefectId:-1 , SourceTableName:'', TargetTableName:'' },
        success: function (result) {
            DefectSummary[0] = result[0].defectTable;
            DefectSummary[1] = result[0].defectRows;
            DefectSummary[2] = result[0].defectData;
            dates[0] = result[0].Date;

            var trendstatsHTML = "Defects Trend (in % across Audit Run Cycles since : " + result[0].Date + ")";
            statsHTML = "Audit Run Statistics displayed for latest available date of: <b>" + result[0].Date + "</b>";

            if (dateParam != null) {
                if (dateParam != result[0].Date) {
                    statsHTML = "Audit Run Statistics displayed for latest available date of : <b>" + result[0].Date + "</b> (Since No Data Exist for the selected Date of : " + dateParam + ")";
                }
            }

            $("#rundate_div1").html(statsHTML);
            google.charts.load('current', { 'packages': ['gauge', 'corechart'] });
            google.charts.setOnLoadCallback(function () { drawGauge("chart_div", labelArray[0], DefectSummary[0]); });
            google.charts.setOnLoadCallback(function () { drawGauge("chart_div2", labelArray[1], DefectSummary[1]); });
            google.charts.setOnLoadCallback(function () { drawGauge("chart_div3", labelArray[2], DefectSummary[2]); });
            google.charts.setOnLoadCallback(function () { drawLines("curve_chart", result, PURPOSE_1, trendstatsHTML); });
        
        }
    });
    
}


function drawGauge(divtg, lbl, defect, test1) {
    var data = google.visualization.arrayToDataTable([
      ['Label', 'Value'],
      [lbl, defect],
    ]);
    var options = {
        width: 500, height: 167,
        redFrom: 20, redTo: 100,
        greenFrom: 0, greenTo: 10,
        yellowFrom: 10, yellowTo: 20,
        minorTicks: 5
    };
    var gaugeChart = new google.visualization.Gauge(document.getElementById(divtg));
    gaugeChart.draw(data, options);

    setInterval(function () {
        data.setValue(0, 1, defect);
        gaugeChart.draw(data, options);
    }, 5000);
}


function drawLines(lineTag, lineResults, forPurose, title_l) {
    
    var data = new google.visualization.DataTable();
    var res;
    data.addColumn('date', 'Audit Run Date');
    if (forPurose == PURPOSE_1) {
        data.addColumn('number', '% of Table having Defects');
    }
    data.addColumn('number', '% of Rows having Defects');
    data.addColumn('number', '% of Data having Defects');

    data.addRows(lineResults.length+1);
    
    for (i = 0; i < lineResults.length; i++) {
       
        res = lineResults[i].Date.split("/");
        
        data.setCell(i, 0, new Date(res[2], res[1] - 1, res[0]));
        if (forPurose == PURPOSE_1) {
            data.setCell(i, 1, lineResults[i].defectTable);
            data.setCell(i, 2, lineResults[i].defectRows);
            data.setCell(i, 3, lineResults[i].defectData);
        } else {           
            data.setCell(i, 1, lineResults[i].defectRows);
            data.setCell(i, 2, lineResults[i].defectData);
        }

    }
    var options = {
        title: title_l,
        curveType: 'function',
        legend: { position: 'bottom' },
        pointSize: 5,
        focusTarget: "category",
        hAxis: { showTextEvery: 1 }
    };
    var chart = new google.visualization.LineChart(document.getElementById(lineTag));
    chart.draw(data, options);
  
}



function openMenu(evt, menuItem) {   
    if (menuItem == "menu2") {
        $("#curve_chart").remove();        
        showDataBase();
    }
   
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++)
        tabcontent[i].style.display = "none";  

    document.getElementById(menuItem).style.display = "block";
}



$(function () {
    $("#tabs").tabs({
        event: "mouseover"
    });
});


$("#pass").mouseover(function () {  
    audit = "success"
    if (PcurrentPage == 1 && passSNo==1)
    fetchData(audit, PcurrentPage, passSNo);
});

$("#fail").mouseover(function () {
    audit = "Failed - Has Defects"
    if (FcurrentPage == 1 && failSNo == 1)
    fetchData(audit, FcurrentPage, failSNo);
});



function showDataBase() {
    document.getElementById('gauges_chart').style.display = "block";

    if (NewpageNo == null && failSNo==1)
    fetchData(audit, FcurrentPage, failSNo);    
    $('#update_fail').on('click', '.footerContent a', function (e) {
        e.preventDefault();
        NewpageNo = parseInt($(this).html());
        failSNo = updatePageNoSNo(NewpageNo,FcurrentPage, failSNo);      
        FcurrentPage = NewpageNo;
        fetchData(audit, FcurrentPage,failSNo);
    });

    $('#update_pass').on('click', '.footerContent a', function (e) {
        e.preventDefault();
         NewpageNo = parseInt($(this).html());
         passSNo = updatePageNoSNo(NewpageNo,PcurrentPage, passSNo);
        PcurrentPage = NewpageNo;
        fetchData(audit, PcurrentPage, passSNo);
    });
}
function updatePageNoSNo(newPage, currentPage, SerialNo) {
    
    if (SerialNo % 5 == 0)
        SerialNo = (SerialNo - 4) + ((newPage - currentPage) * 5);
    else {
        SerialNo = SerialNo + (5-(SerialNo % 5) );
        SerialNo = (SerialNo - 4) + ((newPage - currentPage) * 5);
    }
    return SerialNo;
}

function fetchData(auditResult, pageNo, SNo) {    
    //Create loading panel
    var $loading = "<div class='loading'>Please wait...</div>";
    if(auditResult=="success")
        $('#update_pass').prepend($loading);
    else
        $('#update_fail').prepend($loading);   
    var totalPage;
    summaryTrendUrl = "/api/summaryDefectTrend";
    $.ajax({
        url: '/api/GetSummaryDataApi',       
        data: { audit: auditResult, rundate: dateParam, pageNo: pageNo },
        success: function (d) {
            if (d.length > 0) {
                var $data = $('<table  ></table>').addClass('table table-responsive table-striped ');
                var header = "<thead><tr><th>#</th><th>Audit Direction</th><th>Target Table</th><th>Source Table</th><th>Rows Defect</th><th>Data Defects</th><th>Details</th><th>Comparison</th><th>Trends</th></tr></thead>";
                $data.append(header);              
                $.each(d, function (i, row) {
                    var $row = $('<tr class="rowTable"/>');
                    $row.append($('<td class="columnStyle"/>').html(SNo));
                    $row.append($('<td class="columnStyle"/>').html(row.DetailFileName));
                    $row.append($('<td class="columnStyle"/>').html(row.TargetTableName));
                    $row.append($('<td class="columnStyle"/>').html(row.SourceTableName));
                    $row.append($('<td class="columnStyle"/>').html(row.RowsDefects));
                    $row.append($('<td class="columnStyle"/>').html(row.DataDefects));
                    $row.append($('<td class="columnStyle"/>').html("<a href='#' api='/api/GetDetailsDataApi/?DefectId=" + row.DefectId + "' class='popup'>Details</a>"));
                    $row.append($('<td class="columnStyle"/>').html(""));
                    $row.append($('<td class="columnStyle"/>').html("<a href='#' id='trendApi' trendApi=" + summaryTrendUrl + "/?date=" + row.RunDate + "&DefectId=" + row.DefectId + "&SourceTableName=" + row.SourceTableName + "&TargetTableName=" + row.TargetTableName + " class='popup'>Trend</a>"));
                   
                    $data.append($row);
                    totalPage = row.totalPages;
                    if (auditResult == "success")
                             passSNo = SNo++;
                     else
                         failSNo = SNo++;
                });
            }
            else {
                var $noData = $('<div/>').html('No data found');
                if (auditResult == "success")
                    $('#update_pass').html($noData);
                else
                    $('#update_details').html($noData);
              
            }
           

         
            
            var $footer = $('<tr/>');
         
            var $footerTD = $('<td />').attr('colspan', 9).addClass('footerContent');
           
            if (totalPage > 0) {
                for (var i = 1; i <= totalPage; i++) {
                    var $page = $('<span/>').addClass((i == pageNo) ? "current" : "");
                    $page.html((i == pageNo) ? i : "<a href='#'>" + i + "</a>");
                    $footerTD.append($page);
                }
                $footer.append($footerTD);
            }
            $data.append($footer);
            if (auditResult == "success")
                $('#update_pass').html($data);
            else
                $('#update_fail').html($data);
        },
        error: function () {
            
            alert('Error! Please try again.');
        }
    }).done(function () {
        //remove loading panel after ajax call complete
        $loading = null;
    });

}

$('body').on("click", "a.popup", function () {
    var api = $(this).attr('trendApi');
    var sorcTableName = api.split("=")[3].split("&")[0];
    if (api) {
                modal2 = document.getElementById('id02');
                modal2.style.display = "block";
                $.ajax({
                    url: api,
                    success: function (result) {
                        var trendstatsHTML = "Defects Trend (in % across Audit Run Cycles since : " + result[0].Date + ") for Source Table : " + sorcTableName;
                        google.charts.load('current', { 'packages': ['gauge', 'corechart'] });
                        google.charts.setOnLoadCallback(function () { drawLines("update_tables", result, PURPOSE_2, trendstatsHTML); });

                    }
                });
    }

  
});



$('#update_details').on('click', '.footerContent a', function (e) {
    e.preventDefault();
    NewpageNo = parseInt($(this).html());
    SNoVal = updatePageNoSNo(NewpageNo, pageNoVal, failSNo);
    if (NewpageNo % detailPageLength != 0)
        inum = ((NewpageNo % detailPageLength) + 1) - NewpageNo;
    if (inum < 0)
        inum = inum * (-1) + 2;
    pageNoVal = NewpageNo;
    getDetailsDefects(pageNoVal, failSNo);
});


$(document).ready(function () {
    $('body').on("click", "a.popup", function () {
        api = $(this).attr('api'); 
        if (api)
        {
            document.getElementById('gauges_chart').style.display = "none";
            openMenu(event, 'menu3');
           // getDetailsDefects(pageNoVal, SNoVal);
            getDetailsDefects2();
        }
    });
});
var inum = 1;
var str;
function getDetailsDefects(pageNoVal, SNoVal)
{   
var $loading = "<div class='loading'>Please wait...</div>";   
var totalPage;
if (api) {
    modal = document.getElementById('id01');
    modal.style.display = "block";
    $.ajax({
        url: api,
        data: { pageNo: pageNoVal },
        async: false,
        success: function (d) {
            
            if (d.length > 0) {
                var $data = $('<table cellspacing="0" width="100%"></table>').addClass('table table-responsive table-striped display');
                var header = "<thead><tr><th colspan='2'>Defect Details</th>";
                header = header + "<th colspan='4' style='border: hidden;background-color: white;color: black'>Total Number of Pages to navigate : <u>" + d[0].totalPages + " </u></th></tr></thead>";
               
                header = header + "<thead><tr><th>";

                for (var colCount = 0; colCount < d[0].uniqueColumnsAudited.length; colCount++) {
                    if (d[0].uniqueColumnsAudited[0].split(".").length == 1)
                        header = header + d[0].uniqueColumnsAudited[colCount] + "</th><th>";
                    else
                         header = header + d[0].uniqueColumnsAudited[colCount].split(".")[2] + "</th><th>";
                }
               

                header = header + "Target Column Name</th><th>Target Value</th><th>Source Value</th><th>Row#</th><th>Column#</th></tr></thead>";
               $data.append(header);
                $.each(d, function (i, row) {
                    var $row = $('<tr class="rowTable"/>');
                    var ary = row.UniqueId.split(",");
                    for (var c = 0; c < ary.length; c++) {
                        $row.append($('<td class="columnStyle"/>').html(ary[c]));
                    }
                    $row.append($('<td class="columnStyle" />').html(row.TargetColName));
                    $row.append($('<td class="columnStyle"/>').html(row.TargetValue));
                    $row.append($('<td class="columnStyle"/>').html(row.SourceValue));
                    $row.append($('<td class="columnStyle"/>').html(row.Rows));
                    $row.append($('<td class="columnStyle"/>').html(row.Columns));
                    $data.append($row);
                    totalPage = row.totalPages;
                    
                    SNoVal++;
                });
                AuditedCoulumnsAndStatictics(d[0].DefectId, api);
                $('#update_details').html($data);

            }
            else {
                var $noData = $('<div/>').html('Data Not Found in Database!');
                $('#update_details').html($noData);
                $('#update_stats').html($noData);
            }
            $("#thforPageNav").html(totalPage);
           
           var $footer = $('<tr/>');
             var $footerTD = $('<td />').attr('colspan', 9).addClass('footerContent');
             if (totalPage > 0) {
                
                 
               for (var i=inum ; i <= totalPage; i++) {                  
                   var $page = $('<span/>').addClass((i == pageNoVal) ? "current" : "");
                   if (inum > detailPageLength && i == inum)
                       $page.html("<input  type='number' id='previousPage' placeholder='Page No.' min='1'  ></input>");
                       $page.append((i == pageNoVal) ? i : "<a href='#'>" + i + "</a>");

                       if (i % detailPageLength == 0)
                   {
                       inum = i+1;
                       $page.html("<a href='#' id='nextPage'>"+i+"...</a>");  break;
                   }
                   $footerTD.append($page);
               }
             
            }
           $footerTD.append($page);
           $footer.append($footerTD);
   
         
            $data.append($footer);                               
            $('#update_details').html($data);
        },
        error: function () {
            alert("error in api");
        }
    });
}//close if-statement
}


var Interval, chunks,totalChunk, tableDefectDetails,dataResult;
var dataCol=[];
function getDetailsDefects2() {
    var $loading = "<div class='loading'>Please wait...</div>";
    var totalPage;
   
    if (api) {
       
        $.ajax({
            url: api,
            
         //  url: "/Client/LoadData",
          //  type: "POST", 
            data: { pageNo: 0 },
           
            async: false,
           // dataType: 'json',
            success: function (d)
            {
                var j = JSON.stringify(d)
                alert(j);
                dataResult = d;
              
                
                if (d.length > 0)
                {
                    
                  
                    var $data = $('<table cellspacing="0" width="100%" id="defectDetailTbl"></table>').addClass('table table-responsive table-striped display');
                    var header = "<thead><tr><th colspan='2'>Defect Details</th>";
                    header = header + "<th colspan='4' style='border: hidden;background-color: white;color: black'>Total Number of Pages to navigate : <u>" + d[0].totalPages + " </u></th></tr></thead>";

                    header = header + "<thead><tr><th>";

                    for (var colCount = 0; colCount < d[0].uniqueColumnsAudited.length; colCount++) {
                        if (d[0].uniqueColumnsAudited[0].split(".").length == 1)
                            header = header + d[0].uniqueColumnsAudited[colCount] + "</th><th>";
                        else
                            header = header + d[0].uniqueColumnsAudited[colCount].split(".")[2] + "</th><th>";                        
                    }

                    header = header + "Target Column Name</th><th>Target Value</th><th>Source Value</th><th>Row#</th><th>Column#</th></tr></thead>";
                    $data.append(header);
                   
                    AuditedCoulumnsAndStatictics(d[0].DefectId, api);
                    $('#defect_details').html($data);

                    var array=[];
                    var column = [];
                    for (var i = 0; i < d[0].UniqueIdArray.length; i++)                       
                        column.push({ "data": "UniqueIdArray." + i + "" });
                    column.push({ "data": "TargetColName" });
                    column.push({ "data": "TargetValue" });
                    column.push({ "data": "SourceValue" });
                    column.push({ "data": "Rows" });
                    column.push({ "data": "Columns" });
                    column.push({ "width": "20%" });
                    dataCol = column;
                    chunks = d[0].chunks;
                    totalChunk = d[0].totalChunks;
                     //   Interval=setInterval(getDataByChunks, 5000, d, column);// d[0].DefectId using as achunk number
                       
                    tableDetailsFn();
                    alert("next line");
                  //  if (d[0].DefectId != 0) {
                      //  getDataByChunks();
                   // }
                }
                else {
                    AuditedCoulumnsAndStatictics(0, api);
                    var $noData = $('<div/>').html('Data Not Found in Database!');
                    $('#defect_details').html($noData);
                }
             
           
            },
            error: function (dd) {
                var j = JSON.stringify(dd)
                alert(j);
                alert("error in api");
            }
        });
    }//close if(api)-statement
}
function tableDetailsFn() {
    tableDefectDetails = $('#defectDetailTbl').DataTable({
        "bDeferRender": true,
        //  "deferLoading": 57,
        // "serverSide": true,
        "scrollY": "200px",
        "scrollCollapse": true,
        data: dataResult,//d
        "columns": dataCol,//column,
        "fnDrawCallback": function (oSettings) {
            if (dataResult[0].DefectId != 0) {
              getDataByChunks();
             }
        }
    });
}
function getDataByChunks()
{
    if (chunks == totalChunk) {
       
        return 0;
    }
    else {

        $.ajax({
            url: api,
            data: { pageNo: chunks },

            async: false,
            success: function (response) {
                var j = JSON.stringify(response)
                alert(response[response.length].Rows);

              /*  var jsonObject = JSON.parse(response.d);
                var result = jsonObject.map(function (item) {
                    var result = [];
                    for (var i = 0; i < response.UniqueIdArray.length; i++)
                        column.push({ "data": "UniqueIdArray." + i + "" });
                    result.push(item.Id);
                    result.push(item.Name);
                    result.push(item.Description);
                    result.push(item.Image);
                    result.push(item.Parent);
                    result.push(item.Location);
                    result.push("");
                    return result;
                });*/
              //  myTable.rows.add(result);
              //  myTable.draw();
                chunks = response[0].DefectId;
               // alert(d[0].DefectId)
            }
        });
        return getDataByChunks();
    }
   
}

$('body').change( "a#previousPage", function () {
    var NewpageNo = $('#previousPage').val();
    if (NewpageNo % detailPageLength != 0)
        inum = ((NewpageNo % detailPageLength) + 1) - NewpageNo;
    if (inum < 0)
        inum = inum * (-1) + 2;
    pageNoVal = NewpageNo;
    getDetailsDefects(pageNoVal, failSNo);
   
});



function AuditedCoulumnsAndStatictics(refId, statsUrl) {
   // modal = document.getElementById('id01');
  //  modal.style.display = "block";
    $.ajax({
        url: statsUrl,
        type:"POST",
        success: function (d) {
            if (d.AuditCoverage != null) {
                var $data = $('<table></table>').addClass('table table-responsive table-striped');
               
                var header = "<thead><tr> <th></th><th>Statistics for Rows Audited</th><th> </th><th>Statistics for Data Cells Audited</th><th> </th></tr></thead>";
                $data.append(header);

                    var $row = $('<tr class="rowTable"/>');
                    $row.append($('<td  class="columnStyle" rowspan="4"/>').html("<b>Audit Coverage " + d.AuditCoverage + "%</b>"));
                    $row.append($('<td class="columnStyle"/>').html("Rows Checked"));
                    $row.append($('<td class="columnStyle"/>').html(d.RowChecked));
                    $row.append($('<td class="columnStyle"/>').html("Data Cells Audited"));
                    $row.append($('<td class="columnStyle"/>').html(d.UniqueDataCellAudited));
                    $data.append($row);
                    $row = $('<tr class="rowTable"/>');
                  //  $row.append($('<td  class="columnStyle" rowspan="4"/>').html(d.AuditCoverage));
                    $row.append($('<td class="columnStyle"/>').html("Rows Passed"));
                    $row.append($('<td class="columnStyle"/>').html(d.RowPassed));
                    $row.append($('<td class="columnStyle"/>').html("Data Cells Passed"));
                    $row.append($('<td class="columnStyle"/>').html(d.UniqueDataCellPassed));
                    $data.append($row);
                    $row = $('<tr class="rowTable"/>');
                  //  $row.append($('<td  class="columnStyle" rowspan="4"/>').html(d.AuditCoverage));
                    $row.append($('<td class="columnStyle"/>').html("Missing Rows"));
                    $row.append($('<td class="columnStyle"/>').html(d.MissinRows));
                    $row.append($('<td class="columnStyle"/>').html("Missing Data Cells"));
                    $row.append($('<td class="columnStyle"/>').html(d.UniqueMissingDataCellAudited));
                    $data.append($row);
                    $row = $('<tr class="rowTable"/>');
                  //  $row.append($('<td  class="columnStyle" rowspan="4"/>').html(d.AuditCoverage));
                    $row.append($('<td class="columnStyle"/>').html("Non Missing Rows Error"));
                    $row.append($('<td class="columnStyle"/>').html(d.NonMissinsRowsError));
                    $row.append($('<td class="columnStyle"/>').html("Non Missing Data Cells Error"));
                    $row.append($('<td class="columnStyle"/>').html(d.UniqueNonMissingDataCellError));
                   
                    $data.append($row);
                    $row = $('<tr class="rowTable" />');
                    $row.append($('<td class="columnStyle" rowspan="' + ((d.ColumnsAudited.length / 4)+2) + '" />').html("<b>Target Columns Audited</b>"));
                    $data.append($row);
                    $row = $('<tr class="rowTable" />');
                    
                    var auditsColumns = "";
                    for (i = 0; i < d.ColumnsAudited.length; i++)
                        if (i == 0)
                            auditsColumns = d.ColumnsAudited[i];
                        else
                            auditsColumns= auditsColumns+" , "+ d.ColumnsAudited[i].toUpperCase() ;
                    $row.append($('<td class="columnStyle" colspan="4"/>').html(auditsColumns));
                    $data.append($row);
                    $('#update_stats').html($data);

            }            
            else {
                            var $noData = $('<div/>').html('Data Not Found in Database!');
                            $('#update_stats').html($noData);
                    }
        },
        error: function () {
            alert("error in stats api");
        }
    });

}





    window.onclick = function (event) {
        if (event.target == modal ) 
            modal.style.display = "none";     
        else
            if (event.target == modal2 )
                modal2.style.display = "none";
       
    }
