﻿var pageNoVal = 1, SNoVal = 1, api;
var summaryTrendUrl = "/api/summaryDefectTrend"
var DefectSummary = [];
var dateParam = null;
var labelArray = ["Tables", "Rows", "Data"]
var dates = [];
var jsonResult;
var modal = null, modal2 = null; //pop-up modal
var FcurrentPage = 1;//current page in audit run inspection
var PcurrentPage = 1;//current page in audit run inspection
var audit = "Failed - Has Defects";
var failSNo = 1;//serial number strat from 1 in audit run inspection
var passSNo = 1;//serial number strat from 1 in audit run inspection
var NewpageNo;//switch new page fromm audit run inspection table
var res;
var statsHTML;
var PURPOSE_1 = "DASHBOARD_TREND";
var PURPOSE_2 = "DETAILS_TREND";
showCharts(dateParam);
var detailPageLength = 10;

$(function () {
    $("#datepicker-1").datepicker();
    $("#datepicker-1").on("change", function () {
        dates = new String($(this).val());       
        res = dates.split("/");
        dateParam = res[1] + "/" + res[0] + "/" + res[2];      
        showCharts(dateParam);
        FcurrentPage = failSNo =passSNo= 1;
        fetchData("Failed - Has Defects", FcurrentPage, failSNo);
        fetchData("success", PcurrentPage, passSNo);
    });
});


var actualDate;
function showCharts(dateParam) {
    summaryTrendUrl = "/api/summaryDefectTrend";
    $.ajax({
        url: summaryTrendUrl,
        data: { date: dateParam, DefectId:-1 , SourceTableName:'', TargetTableName:'' },
        success: function (result) {
            DefectSummary[0] = result[0].defectTable;
            DefectSummary[1] = result[0].defectRows;
            DefectSummary[2] = result[0].defectData;
            dates[0] = result[0].Date;

            var trendstatsHTML = "Defects Trend (in % across Audit Run Cycles since : " + result[0].Date + ")";
            statsHTML = "Audit Run Statistics displayed for latest available date of: <b>" + result[0].Date + "</b>";

            if (dateParam != null) {
                if (dateParam != result[0].Date) {
                    statsHTML = "Audit Run Statistics displayed for latest available date of : <b>" + result[0].Date + "</b> (Since No Data Exist for the selected Date of : " + dateParam + ")";
                }
            }

            $("#rundate_div1").html(statsHTML);
            google.charts.load('current', { 'packages': ['gauge', 'corechart'] });
            google.charts.setOnLoadCallback(function () { drawGauge("chart_div", labelArray[0], DefectSummary[0]); });
            google.charts.setOnLoadCallback(function () { drawGauge("chart_div2", labelArray[1], DefectSummary[1]); });
            google.charts.setOnLoadCallback(function () { drawGauge("chart_div3", labelArray[2], DefectSummary[2]); });
            google.charts.setOnLoadCallback(function () { drawLines("curve_chart", result, PURPOSE_1, trendstatsHTML); });
        
        }
    });
    
}


function drawGauge(divtg, lbl, defect, test1) {
    var data = google.visualization.arrayToDataTable([
      ['Label', 'Value'],
      [lbl, defect],
    ]);
    var options = {
        width: 500, height: 167,
        redFrom: 20, redTo: 100,
        greenFrom: 0, greenTo: 10,
        yellowFrom: 10, yellowTo: 20,
        minorTicks: 5
    };
    var gaugeChart = new google.visualization.Gauge(document.getElementById(divtg));
    gaugeChart.draw(data, options);

    setInterval(function () {
        data.setValue(0, 1, defect);
        gaugeChart.draw(data, options);
    }, 5000);
}


function drawLines(lineTag, lineResults, forPurose, title_l) {
   
    var data = new google.visualization.DataTable();
    var res;
    data.addColumn('date', 'Audit Run Date');
    if (forPurose == PURPOSE_1) {
        data.addColumn('number', '% of Table having Defects');
    }
    
    data.addColumn('number', '% of Rows having Defects');
    data.addColumn('number', '% of Data having Defects');

    data.addRows(lineResults.length+1);
    
    for (i = 0; i < lineResults.length; i++) {
       
        if (lineResults[i] != null) {
            res = lineResults[i].Date.split("/");

            data.setCell(i, 0, new Date(res[2], res[1] - 1, res[0]));
            if (forPurose == PURPOSE_1) {
                data.setCell(i, 1, lineResults[i].defectTable);
                data.setCell(i, 2, lineResults[i].defectRows);
                data.setCell(i, 3, lineResults[i].defectData);
            } else {
                data.setCell(i, 1, lineResults[i].defectRows);
                data.setCell(i, 2, lineResults[i].defectData);
            }
        }
    }
    var options = {
        title: title_l,
        curveType: 'function',
        legend: { position: 'bottom' },
        pointSize: 5,
        focusTarget: "category",
        hAxis: { showTextEvery: 1 }
    };
    var chart = new google.visualization.LineChart(document.getElementById(lineTag));
    chart.draw(data, options);
  
}


function openMenu(evt, menuItem) {   
    if (menuItem == "menu2") {
        $("#curve_chart").remove();        
        showDataBase();
    }
    if (menuItem == "menu3") {
        timeoutLoader = setInterval(progressSim, 1000);//1sec   

    }
   
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++)
        tabcontent[i].style.display = "none";  

    document.getElementById(menuItem).style.display = "block";
}



$(function () {
    $("#tabs").tabs({
        event: "mouseover"
    });
});


$("#pass").mouseover(function () {  
    audit = "success"
    if (PcurrentPage == 1 && passSNo==1)
    fetchData(audit, PcurrentPage, passSNo);
});

$("#fail").mouseover(function () {
    audit = "Failed - Has Defects"
    if (FcurrentPage == 1 && failSNo == 1)
    fetchData(audit, FcurrentPage, failSNo);
});



function showDataBase() {
    document.getElementById('gauges_chart').style.display = "block";

    if (NewpageNo == null && failSNo==1)
    fetchData(audit, FcurrentPage, failSNo);    
    $('#update_fail').on('click', '.footerContent a', function (e) {
        e.preventDefault();
        NewpageNo = parseInt($(this).html());
        failSNo = updatePageNoSNo(NewpageNo,FcurrentPage, failSNo);      
        FcurrentPage = NewpageNo;
        fetchData(audit, FcurrentPage,failSNo);
    });

    $('#update_pass').on('click', '.footerContent a', function (e) {
        e.preventDefault();
         NewpageNo = parseInt($(this).html());
         passSNo = updatePageNoSNo(NewpageNo,PcurrentPage, passSNo);
        PcurrentPage = NewpageNo;
        fetchData(audit, PcurrentPage, passSNo);
    });
}
function updatePageNoSNo(newPage, currentPage, SerialNo) {
    
    if (SerialNo % 5 == 0)
        SerialNo = (SerialNo - 4) + ((newPage - currentPage) * 5);
    else {
        SerialNo = SerialNo + (5-(SerialNo % 5) );
        SerialNo = (SerialNo - 4) + ((newPage - currentPage) * 5);
    }
    return SerialNo;
}

function fetchData(auditResult, pageNo, SNo) {    
    //Create loading panel
    var $loading = "<div class='loading'>Please wait...</div>";
    if(auditResult=="success")
        $('#update_pass').prepend($loading);
    else
        $('#update_fail').prepend($loading);   
    var totalPage;
    summaryTrendUrl = "/api/summaryDefectTrend";
    $.ajax({
        url: '/api/GetSummaryDataApi',       
        data: { audit: auditResult, rundate: dateParam, pageNo: pageNo },
        success: function (d) {
            if (d.length > 0) {
                var $data = $('<table  ></table>').addClass('table table-responsive table-striped ');
                var header = "<thead><tr><th>#</th><th>Audit Direction</th><th>Target Table</th><th>Source Table</th><th>Rows Defect</th><th>Data Defects</th><th>Details</th><th>Comparison</th><th>Trends</th></tr></thead>";
                $data.append(header);              
                $.each(d, function (i, row) {
                    var $row = $('<tr class="rowTable"/>');
                    $row.append($('<td class="columnStyle"/>').html(SNo));
                    $row.append($('<td class="columnStyle"/>').html(row.DetailFileName));
                    $row.append($('<td class="columnStyle"/>').html(row.TargetTableName));
                    $row.append($('<td class="columnStyle"/>').html(row.SourceTableName));
                    $row.append($('<td class="columnStyle"/>').html(row.RowsDefects));
                    $row.append($('<td class="columnStyle"/>').html(row.DataDefects));
                    $row.append($('<td class="columnStyle"/>').html("<a href='#' api='/api/GetDetailsDataApi/?DefectId=" + row.DefectId + "' class='popup'>Details</a>"));
                    $row.append($('<td class="columnStyle"/>').html(""));
                    $row.append($('<td class="columnStyle"/>').html("<a href='#' id='trendApi' trendApi=" + summaryTrendUrl + "/?date=" + row.RunDate + "&DefectId=" + row.DefectId + "&SourceTableName=" + row.SourceTableName + "&TargetTableName=" + row.TargetTableName + " class='popup'>Trend</a>"));
                   
                    $data.append($row);
                    totalPage = row.totalPages;
                    if (auditResult == "success")
                             passSNo = SNo++;
                     else
                         failSNo = SNo++;
                });
            }
            else {
                var $noData = $('<div/>').html('No data found');
                if (auditResult == "success")
                    $('#update_pass').html($noData);
                else
                    $('#update_details').html($noData);
              
            }
           
            var $footer = $('<tr/>');         
            var $footerTD = $('<td />').attr('colspan', 9).addClass('footerContent');           
            if (totalPage > 0) {
                for (var i = 1; i <= totalPage; i++) {
                    var $page = $('<span/>').addClass((i == pageNo) ? "current" : "");
                    $page.html((i == pageNo) ? i : "<a href='#'>" + i + "</a>");
                    $footerTD.append($page);
                }
                $footer.append($footerTD);
            }
            try{
                $data.append($footer);
                if (auditResult == "success")
                    $('#update_pass').html($data);
                else
                    $('#update_fail').html($data);
            }
            catch(err){}
        },
        error: function () {
            
            alert('Error! Please try again.');
        }
    }).done(function () {
        //remove loading panel after ajax call complete
        $loading = null;
    });

}

$('body').on("click", "a.popup", function () {
   
    $(this).css({ "cursor": "wait" });
    var api = $(this).attr('trendApi');
    try {
       
        var sorcTableName = api.split("=")[3].split("&")[0];
    }
    catch (err) { }
   
    if (api) {
               
                modal2 = document.getElementById('id02');
                modal2.style.display = "block";
                $.ajax({
                    url: api,
                    success: function (result) {
                      
                        var trendstatsHTML = "Defects Trend (in % across Audit Run Cycles since : " + result[0].Date + ") for Source Table : " + sorcTableName;
                        google.charts.load('current', { 'packages': ['gauge', 'corechart'] });
                        google.charts.setOnLoadCallback(function () { drawLines("update_tables", result, PURPOSE_2, trendstatsHTML); });

                    }
                });
    }

  
});


/*
$('body').change( "a#previousPage", function () {
    var NewpageNo = $('#previousPage').val();
    if (NewpageNo % detailPageLength != 0)
        inum = ((NewpageNo % detailPageLength) + 1) - NewpageNo;
    if (inum < 0)
        inum = inum * (-1) + 2;
    pageNoVal = NewpageNo;
    getDetailsDefects(pageNoVal, failSNo);
   
});



$('#update_details').on('click', '.footerContent a', function (e) {
    e.preventDefault();
    NewpageNo = parseInt($(this).html());
    SNoVal = updatePageNoSNo(NewpageNo, pageNoVal, failSNo);
    if (NewpageNo % detailPageLength != 0)
        inum = ((NewpageNo % detailPageLength) + 1) - NewpageNo;
    if (inum < 0)
        inum = inum * (-1) + 2;
    pageNoVal = NewpageNo;
    getDetailsDefects(pageNoVal, failSNo);
});



var inum = 1;
var str;
function getDetailsDefects(pageNoVal, SNoVal)
{   
var $loading = "<div class='loading'>Please wait...</div>";   
var totalPage;
if (api) {
    modal = document.getElementById('id01');
    modal.style.display = "block";
    $.ajax({
        url: api,
        data: { pageNo: pageNoVal },
        async: false,
        success: function (d) {
            
            if (d.length > 0) {
                var $data = $('<table cellspacing="0" width="100%"></table>').addClass('table table-responsive table-striped display');
                var header = "<thead><tr><th colspan='2'>Defect Details</th>";
                header = header + "<th colspan='4' style='border: hidden;background-color: white;color: black'>Total Number of Pages to navigate : <u>" + d[0].totalPages + " </u></th></tr></thead>";
               
                header = header + "<thead><tr><th>";

                for (var colCount = 0; colCount < d[0].uniqueColumnsAudited.length; colCount++) {
                    if (d[0].uniqueColumnsAudited[0].split(".").length == 1)
                        header = header + d[0].uniqueColumnsAudited[colCount] + "</th><th>";
                    else
                         header = header + d[0].uniqueColumnsAudited[colCount].split(".")[2] + "</th><th>";
                }
               

                header = header + "Target Column Name</th><th>Target Value</th><th>Source Value</th><th>Row#</th><th>Column#</th></tr></thead>";
               $data.append(header);
                $.each(d, function (i, row) {
                    var $row = $('<tr class="rowTable"/>');
                    var ary = row.UniqueId.split(",");
                    for (var c = 0; c < ary.length; c++) {
                        $row.append($('<td class="columnStyle"/>').html(ary[c]));
                    }
                    $row.append($('<td class="columnStyle" />').html(row.TargetColName));
                    $row.append($('<td class="columnStyle"/>').html(row.TargetValue));
                    $row.append($('<td class="columnStyle"/>').html(row.SourceValue));
                    $row.append($('<td class="columnStyle"/>').html(row.Rows));
                    $row.append($('<td class="columnStyle"/>').html(row.Columns));
                    $data.append($row);
                    totalPage = row.totalPages;
                    
                    SNoVal++;
                });
                AuditedCoulumnsAndStatictics(d[0].DefectId, api);
                $('#update_details').html($data);

            }
            else {
                var $noData = $('<div/>').html('Data Not Found in Database!');
                $('#update_details').html($noData);
                $('#update_stats').html($noData);
            }
            $("#thforPageNav").html(totalPage);
           
           var $footer = $('<tr/>');
             var $footerTD = $('<td />').attr('colspan', 9).addClass('footerContent');
             if (totalPage > 0) {
                
                 
               for (var i=inum ; i <= totalPage; i++) {                  
                   var $page = $('<span/>').addClass((i == pageNoVal) ? "current" : "");
                   if (inum > detailPageLength && i == inum)
                       $page.html("<input  type='number' id='previousPage' placeholder='Page No.' min='1'  ></input>");
                       $page.append((i == pageNoVal) ? i : "<a href='#'>" + i + "</a>");

                       if (i % detailPageLength == 0)
                   {
                       inum = i+1;
                       $page.html("<a href='#' id='nextPage'>"+i+"...</a>");  break;
                   }
                   $footerTD.append($page);
               }
             
            }
           $footerTD.append($page);
           $footer.append($footerTD);
   
         
            $data.append($footer);                               
            $('#update_details').html($data);
        },
        error: function () {
            alert("error in api");
        }
    });
}//close if-statement
}
*/
var snackbar;
$(document).ready(function () {
    $('body').on("click", "a.popup", function () {
        $(function () {
            al = 0.00; percentage = 0.00; diff = 0;
            start = 4.72;
            loop = 0;
            modalLoader = null, timeoutLoader = setInterval(progressSim, 1000);

            snackbar = document.getElementById("snackbar")
            snackbar.className = "show";
            
        });
        api = $(this).attr('api'); //api='/api/GetDetailsDataApi/?DefectId="
        if (api)
        {           
            document.getElementById('gauges_chart').style.display = "none";
            openMenu(event, 'menu3');
           // getDetailsDefects(pageNoVal, SNoVal);
            getDetailsDefects2();
            clearTimeout(triggerTimeOut);
          
               
        }
    });
});

var Interval, chunks,totalChunk, tableDefectDetails,dataResult,totalRows,downloadRows=0, selectBox;
var dataCol = [];
var filters = {};
filters.options = [];

function getDetailsDefects2() {
    var $loading = "<div class='loading'>Please wait...</div>";
    var totalPage;
    $('#filterTable').remove();
   
    if (api) {
       
        $.ajax({
            url: api,//api='/api/GetDetailsDataApi/?DefectId=21..."
            data: { pageNo: 0 },//using pageNo as chunks            
            async: false,           
            success: function (d)
            {               
                dataResult = d;     
                if (d.length > 0)
                {          
                    var $data = $('<table cellspacing="0" width="100%" id="defectDetailTbl" data-scroll-x="true"  scroll-collapse="false"></table>').addClass('table table-bordered table-responsive table-striped display');
                   /* var header1 = "<thead id='theadId'><tr>";
                    header1 = header1 + "<th colspan='4' style='border: hidden;background-color: white;color: black'>Total Number of Pages to navigate : <u>" + d[0].totalPages + " </u></th></tr></thead>";*/
                    var header = header + "<thead><tr><th id='";
                    for (var colCount = 0; colCount < d[0].uniqueColumnsAudited.length; colCount++)
                    {
                        if (d[0].uniqueColumnsAudited[0].split(".").length == 1)
                            header = header + d[0].uniqueColumnsAudited[colCount] + "</th><th>";
                        else
                            header = header + d[0].uniqueColumnsAudited[colCount].split(".")[2] + "_id' href='#'>" + d[0].uniqueColumnsAudited[colCount].split(".")[2] + "</th><th id='";                                                 
                    }
                    header = header + "Target_Column_Name_id'>Target_Column_Name</th><th id='Target_Value_id'>Target_Value</th><th id='Source_Value_id'>Source_Value</th><th id='RowNo_id'>RowNo</th><th id='ColNo_id'>ColNo</th></tr></thead>";
                    $data.append(header);


                    selectBox = "<table id='filterTable' class='table table-bordered table-responsive table-striped display'><tr><td><select  id='";
                    for (var colCount = 0; colCount < d[0].uniqueColumnsAudited.length; colCount++) {
                        if (d[0].uniqueColumnsAudited[0].split(".").length == 1)
                            selectBox = selectBox + d[0].uniqueColumnsAudited[colCount] + "</th><th>";
                        else
                            selectBox = selectBox + d[0].uniqueColumnsAudited[colCount].split(".")[2] + "_id' href='#' style='width:" + d[0].uniqueColumnsAudited[colCount].split(".")[2].length * 10 +"px'><option value=''>" + d[0].uniqueColumnsAudited[colCount].split(".")[2] + "</option></select></td><td><select  id='";                      
                    }
                    selectBox = selectBox + "TARGET_COLUMN_NAME_id' style='width:180px'><option value=''>TARGET_COLUMN_NAME</option></select></td><td><select id='TARGET_VALUE_id'  style='width:120px'><option value='Target Value'>TARGET_VALUE</option></select></td><td><select id='SOURCE_VALUE_id'  style='width:120px'><option value='Source Value'>SOURCE_VALUE</option></select></td><td><select id='ROWNO_id'  style='width:50px'><option value='Rows#'>ROWNO</option></select></td><td><select id='COLNO_id'  style='width:50px'><option value='Column#'>COLNO</option></select></td></tr></table>";
                    $('#defect_details_Filters').append($(selectBox));
                                            
                    AuditedCoulumnsAndStatictics(d[0].DefectId, api);
                    $('#defect_details').html($data);
                                  
                    var column = [];
                    var i;
                    for (i = 0; i < d[0].UniqueIdArray.length; i++)                       
                        column.push({ "data": "UniqueIdArray." + i + "" });
                    column.push({ "data": "TargetColName" });
                    column.push({ "data": "TargetValue" });
                    column.push({ "data": "SourceValue" });
                    column.push({ "data": "Rows" });
                    column.push({ "data": "Columns" });                
                    dataCol = column;
                    chunks = d[0].chunks;                   
                    totalChunk = d[0].totalChunks;
                    totalRows = d[0].totalPages;//using 'totalPages' as 'totalRows' in database to sync uploader bar
                    downloadRows = d.length;

                    tableDetailsFn();
                    $(this).css({ "cursor": "wait" });
                    Interval = setTimeout(getDataByChunks, 10000);//setInterval
                    
                }
                else {
                    AuditedCoulumnsAndStatictics(0, api);
                    var $noData = $('<div/>').html('Data Not Found in Database!');
                    $('#defect_details').html($noData);
                }             
           
            },
            error: function (dd) {
                var j = JSON.stringify(dd)
                alert("error in api : "+j);               
            }
        });       
    }//close if(api)-statement
    $('#defectDetailTbl_filter').remove();
}



var column, select;
var index; 
var show = true,onChange=false;
function tableDetailsFn() {
    tableDefectDetails = $('#defectDetailTbl').DataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        "bAutoWidth": false,
        "bDeferRender": true,
        "autoWidth": true,
        "scrollY": "900px",
        "scrollCollapse": true,
        data: dataResult,//d
        "columns": dataCol,//column, 
        initComplete: function () {            
            this.api().columns().every(function () {
                column = this;
                column.search('').draw();
               // select = $('<select id="selectBox"><option value=""></option></select>').appendTo($(column.footer()).empty());
             
            });
        },

    });

    $('#resetDataTable').click(function () {
        column.search('').draw();
    });
    $('#filterTable td select').mouseenter(function () {
        chunks = 0;
        $(this).css({ "cursor": "wait" });
    });
    var timeOut;   
    $('#filterTable td').click(function () {//mouseenter       

        var colName, indexCol=null;
        try {
            indexCol = $(this).index().valueOf();
            colName = dataResult[0].uniqueColumnsAudited[$(this).parent().children().index($(this))].split(".")[2];
          
          
        }
        catch (err) {
           
           
            switch(indexCol) {
                case dataResult[0].uniqueColumnsAudited.length:
                     colName = 'TARGET_COLUMN_NAME';
                    break;
                case dataResult[0].uniqueColumnsAudited.length+1:
                    colName = 'TARGET_VALUE';
                    break;
                case dataResult[0].uniqueColumnsAudited.length+2:
                    colName = 'SOURCE_VALUE';
                    break;
                case dataResult[0].uniqueColumnsAudited.length+3:
                    colName = 'ROWNO';
                    break;
                case dataResult[0].uniqueColumnsAudited.length+4:
                    colName = 'COLNO';
                    break;
                default:
                    colName = dataResult[0].uniqueColumnsAudited[$(this).parent().children().index($(this))].split(".")[2];
               
            }
         
        }
      
    
        if (show) {
            index = $(this).index() + ':visIdx';//$(this).parent().children().index($(this))            
            tableDefectDetails.column(index).each(function () {
                column = this;
                select = initSelectBox(colName);
                $('#filterTable td select').css({ "cursor": "wait" });
                select.css({ "width":colName.length*10 });
                onChange = false;
                select.on('change', function () {
                    onChange = true;
                    var val = $.fn.dataTable.util.escapeRegex($(this).val());
                    show = true;
                    column.search(val ? '^' + val + '$' : '', true, false).draw();
                });

            });
            $.when(loadDataColfn());
           
            function loadDataColfn() {
                var data = tableDefectDetails.column(index).data().unique();
                select.append('<option value="' + data[0] + '">' + data[0] + '</option>');
                data.splice(0, 1); // we only want remaining data
                var appendOptions = function () {
                    show = false;
                    var dataChunk = data.splice(0, 1000); // configure this last number (the size of the 'chunk') to suit your needs               
                    for (var i = 0; i < dataChunk.length; i++) {
                        select.append('<option value="' + dataChunk[i] + '">' + dataChunk[i] + '</option>');
                        if (i == dataChunk.length - 1)
                            $(this).removeAttr("cursor");
                    }

                    if (data.count() > 0) {
                        timeOut = setTimeout(appendOptions, 100); // change time to suit needs   setInterval
                    }
                };
                appendOptions(); // kicks it off
                //  tableDefectDetails.column($(this).index() + ':visIdx').data().unique().each(function (d, j) {              
                //     select.append('<option value="' + d + '">' + d + '</option>');
                //      show = false;              
                //  });

            }

        }//if-statement
       
        $('#filterTable td').mouseleave(function () { 
            $(this).css({ "cursor": "wait" });
            clearTimeout(timeOut);          
            select = initSelectBox(colName);
            show = true;
        });
    });
  

}

function initSelectBox(colName)
{
    //return $('<select  id="selectBox" class="chosen-select"  ></select>').appendTo($(column.footer()).empty());//<option  value=""  selected="selected"></option>
    return $('#' + colName + '_id').empty().append('<option value="">' + colName + '</option>').append('<option value=""></option>');
}
   

var triggerTimeOut;
function getDataByChunks()
{
    modalLoader.style.display = "block";
    document.getElementById('menu4').style.display = 'none';
  
    if (chunks == totalChunk) { 
        $.when(tableDefectDetails.draw());
        //  tableDefectDetails.columns.adjust().draw();
        chunks++;
        clearTimeout(Interval);      
        clearTimeout(triggerTimeOut);
      
    }
    else {
        chunks++;
       
        $.ajax({
            url: api,//api='/api/GetDetailsDataApi/?DefectId="
            data: { pageNo: chunks },
            async: false,
            success: function (response) {              
                if (chunks > 6)
                    triggerTimeOut = setInterval(function () { $("#update_stats").trigger("click"); }, 1000);
                downloadRows = response.length;             
                tableDefectDetails.rows.add(response);
                
                $.when(tableDefectDetails.draw()).done(function () {
                    modalLoader.style.display = "none";
                    document.getElementById('menu4').style.display = 'block';
                    setTimeout(getDataByChunks, (1) * 100);
                    setTimeout(function () { snackbar.className = snackbar.className.replace("show", ""); },chunks* 60000);
                });
             
            },
            error: function (dd) {
                alert("error");
           
            }
             
        });
        
    }
   
}

function AuditedCoulumnsAndStatictics(refId, statsUrl) {
   // modal = document.getElementById('id01');
  //  modal.style.display = "block";
    $.ajax({
        url: statsUrl,
        type:"POST",
        success: function (d) {
            if (d.AuditCoverage != null) {
                var $data = $('<table></table>').addClass('table table-responsive table-striped');
               
                var header = "<thead><tr> <th></th><th>Statistics for Rows Audited</th><th> </th><th>Statistics for Data Cells Audited</th><th> </th></tr></thead>";
                $data.append(header);

                    var $row = $('<tr class="rowTable"/>');
                    $row.append($('<td  class="columnStyle" rowspan="4"/>').html("<b>Audit Coverage " + d.AuditCoverage + "%</b>"));
                    $row.append($('<td class="columnStyle"/>').html("Rows Checked"));
                    $row.append($('<td class="columnStyle"/>').html(d.RowChecked));
                    $row.append($('<td class="columnStyle"/>').html("Data Cells Audited"));
                    $row.append($('<td class="columnStyle"/>').html(d.UniqueDataCellAudited));
                    $data.append($row);
                    $row = $('<tr class="rowTable"/>');
                  //  $row.append($('<td  class="columnStyle" rowspan="4"/>').html(d.AuditCoverage));
                    $row.append($('<td class="columnStyle"/>').html("Rows Passed"));
                    $row.append($('<td class="columnStyle"/>').html(d.RowPassed));
                    $row.append($('<td class="columnStyle"/>').html("Data Cells Passed"));
                    $row.append($('<td class="columnStyle"/>').html(d.UniqueDataCellPassed));
                    $data.append($row);
                    $row = $('<tr class="rowTable"/>');
                  //  $row.append($('<td  class="columnStyle" rowspan="4"/>').html(d.AuditCoverage));
                    $row.append($('<td class="columnStyle"/>').html("Missing Rows"));
                    $row.append($('<td class="columnStyle"/>').html(d.MissinRows));
                    $row.append($('<td class="columnStyle"/>').html("Missing Data Cells"));
                    $row.append($('<td class="columnStyle"/>').html(d.UniqueMissingDataCellAudited));
                    $data.append($row);
                    $row = $('<tr class="rowTable"/>');
                  //  $row.append($('<td  class="columnStyle" rowspan="4"/>').html(d.AuditCoverage));
                    $row.append($('<td class="columnStyle"/>').html("Non Missing Rows Error"));
                    $row.append($('<td class="columnStyle"/>').html(d.NonMissinsRowsError));
                    $row.append($('<td class="columnStyle"/>').html("Non Missing Data Cells Error"));
                    $row.append($('<td class="columnStyle"/>').html(d.UniqueNonMissingDataCellError));
                   
                    $data.append($row);
                    $row = $('<tr class="rowTable" />');
                    $row.append($('<td class="columnStyle" rowspan="' + ((d.ColumnsAudited.length / 4)+2) + '" />').html("<b>Target Columns Audited</b>"));
                    $data.append($row);
                    $row = $('<tr class="rowTable" />');
                    
                    var auditsColumns = "";
                    for (i = 0; i < d.ColumnsAudited.length; i++)
                        if (i == 0)
                            auditsColumns = d.ColumnsAudited[i];
                        else
                            auditsColumns= auditsColumns+" , "+ d.ColumnsAudited[i].toUpperCase() ;
                    $row.append($('<td class="columnStyle" colspan="4"/>').html(auditsColumns));
                    $data.append($row);
                    $('#update_stats').html($data);

            }            
            else {
                            var $noData = $('<div/>').html('Data Not Found in Database!');
                            $('#update_stats').html($noData);
                    }
        },
        error: function () {
            alert("error in stats api");
        }
    });

}

var al = 0.00, percentage = 0.00, diff=0.1;
var start = 4.72;// start from 12'o-clock
var ctx = document.getElementById('my_canvas2').getContext('2d');
var cw = ctx.canvas.width;
var ch = ctx.canvas.height;
var loop = 0;
var modalLoader = null, timeoutLoader;
function progressSim() {
    if (diff == 0) {
        modalLoader = document.getElementById('menuloader');
        modalLoader.style.display = "block";
        ctx.fillText('please wait...', cw * .5, ch * .65 - 2, cw);

    }
    diff = (((al) / 100) * Math.PI * 2 * 10).toFixed(2);
    ctx.clearRect(0, 0, cw, ch);
    ctx.lineWidth = 20;
    ctx.fillStyle = '#80b7f4';
    ctx.strokeStyle = "#b3ffb1";
    ctx.textAlign = 'center';
    ctx.font = '20px Arial';

    ctx.beginPath();

    ctx.arc(500, 170, 100, start, diff / 10 + start, false);
    ctx.stroke();

    percentage = Math.round((downloadRows / totalRows) * 100) / 100;
   
    diff = percentage - al;
    ctx.fillText(al.toFixed(2) + '%', cw * .5, ch * .33 - 2, cw);
    if (chunks >= (totalChunk - 1)  )
    {
        if (chunks == (totalChunk - 1) || totalChunk == 1)
            timeoutLoader = setInterval(progressSim, 50);
        al += 1;
    }
    else
        al += diff / 10;
  
    if (al > 100)
    {  //fileBytes < 0 ||
        ctx.fillText(percentage + '% please wait...', cw * .5, ch * .65 - 2, cw);
        clearTimeout(timeoutLoader);       
        modalLoader.style.display = "none";
        document.getElementById('menu4').style.display = 'block';
        modalLoader.style.display = "none";        
    }

    loop++;
}



window.onclick = function (event) {
        if (event.target == modal ) 
            modal.style.display = "none";     
        else
            if (event.target == modal2 )
                modal2.style.display = "none";
    }
