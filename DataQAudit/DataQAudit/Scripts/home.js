﻿/// <reference path="home.js" />
var ctx = document.getElementById('my_canvas').getContext('2d');
var al = 0.00;
var loop = 1;
var fileBytes=1;
var apiCount = 8;
var start = 4.72;//
var cw = ctx.canvas.width;
var ch = ctx.canvas.height;
var diff;
var percentage = 0.00;

function progressSim() {   
    document.getElementById('menu1').style.display = "none";

    diff = (((al) / 100) * Math.PI * 2 * 10).toFixed(2);
    ctx.clearRect(0, 0, cw, ch);
    ctx.lineWidth =20;
    ctx.fillStyle = '#80b7f4';
    ctx.strokeStyle = "#b3ffb1";
    ctx.textAlign = 'center';
    ctx.font = '20px Arial';
   
    ctx.beginPath();
   
    ctx.arc(500, 170, 100, start, diff / 10 + start, false);
    ctx.stroke();

    if (loop>100)//loop % 100 ==0
    {
        if (true) {//loop >= 200
            $.ajax({
                url: "/api/GetSummaryDataApi",
                data: { id: 17 },
                success: function (result) {                   
                    if (result[0] != null && result[1] != null) {
                        fileBytes = result[0];
                        percentage = Math.round((100 - (result[0] / result[1]) * 100) * 100) / 100;  
                    }
                }
            });
         
        }
    }
    diff = percentage -al ;
    ctx.fillText(al.toFixed(2) + '%', cw * .5, ch * .33 - 2, cw);

     al += diff/10;
 
     if (al > 100) {//fileBytes < 0 ||
        ctx.fillText(percentage+'% please wait...', cw * .5, ch * .65 - 2, cw);
            clearTimeout(sim);
        }
        
            
 if(diff==0)
     ctx.fillText('please wait...', cw * .5, ch * .65 - 2, cw);

    loop++;
}


var sim=0;
function openCity(evt, cityName) { 

    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("list-group-item");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    document.getElementById(cityName).style.display = "block";
    
   
     sim = setInterval(progressSim, 100);//1sec
    
   
}


var modal2=null;
function openImg() {
   modal2 = document.getElementById('iid02');
    modal2.style.display = "block";

}


window.onclick = function (event) {
 
        if (event.target == modal2)
            modal2.style.display = "none";

}






