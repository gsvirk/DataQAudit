﻿using DataQAudit.Models;
using DataQAudit.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DataQAudit.Controllers.APIcontrollers
{
    
    public class GetDetailsDataApiController : ApiController
    {
        
        DefectModel detailsDataObj = new DefectModel();
      
        public DefectDetailsData[] GetDetailsData(int defectId,int pageNo)
        {
            return detailsDataObj.getDetailsDataMethod2(defectId, pageNo);
        }

      

        [HttpPost]
        public tablesAuditedRunStats GetDetailsDat(int defectId)
        {
            return detailsDataObj.getDetailsStats(defectId);
        }



    }
}
