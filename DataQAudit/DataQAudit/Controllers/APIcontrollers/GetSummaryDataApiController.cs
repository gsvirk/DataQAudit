﻿using DataQAudit.Models;
using DataQAudit.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DataQAudit.Controllers
{
    public class GetSummaryDataApiController : ApiController
    {
        DefectModel getLatestDefectObj = new DefectModel();
        swabzEntities entity = new swabzEntities();
        // GET api/<controller>
        public DefectSummaryData[] GetSummaryData(string audit,string rundate, int pageNo)
        {        
           
            return getLatestDefectObj.getSummaryDataMethod(audit, rundate, pageNo);
        }

        public int[] getTotalFilebytes(int id)
        {
            int[] bytes = new int[2];
           
            try
            {
                var workSpace = entity.userWorksapces.Where(a => a.id.Equals(17)).FirstOrDefault();
                bytes[0] = (int)workSpace.defectId;//variable
                bytes[1] = Int32.Parse(workSpace.msg.ToString());//fixed           
                return bytes;
            }
            catch (Exception e) { return bytes; }
        }
    }
}
