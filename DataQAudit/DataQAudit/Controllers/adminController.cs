﻿using DataQAudit;
using Newtonsoft.Json;
using pocReport.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace pocReport.Controllers
{
    public class adminController : Controller
    {
        swabzEntities entity = new swabzEntities(); //SWABZ DATABASE OBJECT
        static string userId = null;
        byte[] fileByte;
        
        // GET: admin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult login(user u, HttpPostedFileBase filename)
        {
            
            try
            {
                fileByte = new byte[filename.ContentLength];
            }
            catch (Exception e)
            {

                TempData["msg"] = "Please Upload The Key";
                return RedirectToAction("login", "Home");
            }

           
            var data = filename.InputStream.Read(fileByte, 0, Convert.ToInt32(filename.ContentLength));
            string publicKey = System.Text.Encoding.UTF8.GetString(fileByte);
            
            var userIdPwd = entity.users.Where(a => a.userId.Equals(u.userId) && a.pwd.Equals(u.pwd)).FirstOrDefault();

            var domainKey = entity.domains.Where(a => a.domain1.Equals(u.domain.domain1) && a.publicKey.Equals(publicKey)).FirstOrDefault();

            if (ModelState.IsValid)
            { //this is a check validity              
                if (userIdPwd != null && domainKey != null && userIdPwd.userId.Equals("admin") && userIdPwd.pwd.Equals("admin"))
                {
                    Session["LoggedUserId"] = userIdPwd.userId.ToString();
                    Session["LoggedCompany"] = domainKey.domain1.ToString();
                    userId = userIdPwd.userId.ToString();
                    return RedirectToAction("admin");
                }
                else
                    if (userIdPwd != null && domainKey != null)
                {
                    Session["LoggedUserId"] = userIdPwd.userId.ToString();
                    Session["LoggedCompany"] = domainKey.domain1.ToString();
                    string key = publicKey;
                    string decryptLogin = encryptDecrypt.Decrypt(key);
                    var pblicKey = entity.domains.Where(a => a.publicKey.Equals(key)).FirstOrDefault();
                    string decryptEntity = encryptDecrypt.Decrypt(pblicKey.publicKey.ToString());
                    /*****************************encrypting company id to open client page/report page*******************************************************/
                    string comapnyID = userIdPwd.companyId.ToString();
                    string encryptCompanyId = encryptDecrypt.Encrypt(comapnyID);
                    if (decryptEntity.Equals(decryptLogin))
                        return RedirectToAction("clients", "Client", new { userId = userIdPwd.userId, password = encryptCompanyId });//return RedirectToAction("actionName","controllerName");

                    else
                    {
                        TempData["msg"] = "Invalid Key";
                        return RedirectToAction("login", "Home");

                    }
                }
                else
                {
                    if (userIdPwd==null)
                        TempData["msg"] = "Invalid Username and Password";
                    if (domainKey==null)
                        TempData["msg"] = "Invalid Domain";
                    return RedirectToAction("login", "Home");
                }
            }
            return View(u);
         
        }

     
        public ActionResult admin() //showing the list of users
        {
            if (Session["LoggedUserId"] != null)
            {
                List<user> allContacts = new List<user>();
                var v = (from a in entity.users
                         join b in entity.domains on a.companyId equals b.id
                         select new
                         {
                             a,
                             b.clientName,
                             b.domain1,
                             b.id,
                             b.publicKey
                         });
                if (v != null)
                {
                    foreach (var i in v)
                    {
                        user c = i.a;
                        c.clientName = i.clientName;
                        c.domain1 = i.domain1;
                        c.publicKey = i.publicKey;
                        allContacts.Add(c);
                    }
                }
                return View(allContacts);
            }
            else
                return RedirectToAction("Index");
        }
        
        public user getUserById(int id) {
            user user = null;
            using (entity) {
                var v = (from usr in entity.users
                         join company in entity.domains on usr.companyId equals company.id
                         where usr.id.Equals(id)
                         select new
                         {
                             usr,
                             company.clientName,
                             company.domain1,
                             company.publicKey
                         }).FirstOrDefault();
                if (v!=null) {
                    user = v.usr;
                           user.clientName = v.clientName;
                        user.domain1 = v.domain1;
                       user.publicKey = v.publicKey;
                }
                return user;
            }                
        }

        public ViewResult crudOperation(int id =0) {
            if (id > 0)
            {
                var user = getUserById(id);                
                return View(user);
            }

            return View();
        }

      
        public ActionResult delete(int id)
        {
          
            if (id > 0)
            {
                var user = entity.users.Where(a => a.id.Equals(id)).FirstOrDefault();
                entity.users.Remove(user);
                entity.SaveChanges();
                return RedirectToAction("admin");   
            }

            return RedirectToAction("admin");
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult save(user c)
        {            
            bool status = false;
            if (ModelState.IsValid)
            {
                using (entity)
                {
                  
                    if (c.id > 0)
                    {
                        var v = entity.users.Where(a => a.id.Equals(c.id)).FirstOrDefault();
                       
                        if (v != null)
                        {
                            v.userId = c.userId;
                            v.pwd = c.pwd;
                            v.emailId = c.emailId;
                            v.companyId = c.companyId;                            
                        }
                        else
                            return HttpNotFound();
                    }
                    else
                    {
                        entity.users.Add(c);

                    }
                    entity.SaveChanges();

                    var user = entity.users.Where(a => a.userId.Equals(c.userId) && a.companyId.Equals(c.companyId)).FirstOrDefault();
                    userWorksapce workSpace = new userWorksapce();
                    workSpace.userId = user.userId;
                    workSpace.id = user.id;
                    entity.userWorksapces.Add(workSpace);


                    status = true;
                    return RedirectToAction("admin");
                }
            }
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult addDomain(domain c)
        {
            if (ModelState.IsValid)
            {
                using (entity)
                {
                    string publicKey = c.publicKey.ToString();
                    string encryptKey = encryptDecrypt.Encrypt(publicKey);
                    c.publicKey = encryptKey;
                    entity.domains.Add(c);                    
                    entity.SaveChanges();                    
                    return RedirectToAction("admin");
                }
            }
            return RedirectToAction("login");
        }
        public ActionResult addDomain()
        {
            if (ModelState.IsValid)
            {
                return View();              
                
            }
            return RedirectToAction("admin");
        }

    }
}