﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using pocReport.Models;
using Excel;
using System.Data;
using OfficeOpenXml;
using System.Data.OleDb;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Text;
using DataQAudit;
using System.Web.UI;
using DataQAudit.Models;

namespace pocReport.Controllers
{
    public class ClientController : Controller
    {
        swabzEntities entity = new swabzEntities();
        static string encryptCompanyId =null;
       
        // GET: Client
        public PartialViewResult importExcel()
        {
                return PartialView("excelView");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> uploadExcel(IEnumerable<HttpPostedFileBase> files)
        {
           
            asyncUploadExcel obj = new asyncUploadExcel();
         
            foreach (var file in files)
            {
                if (file != null && file.ContentLength > 0)
                {   //  var fileName =Path.GetExtension(file.FileName);
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/Excel"), fileName);
                    file.SaveAs(path);
                }
            }

          


            Task<StringBuilder> statuss =  obj.UploadExcelDataAsync(files, encryptCompanyId);

            StringBuilder status = await statuss;
            if (status!=null)
            {
                 TempData["msg"] = "<script>alert('" + status.ToString() + "');</script>";
             //   TempData["msg"] = "<script> $('#alerts').html(" + status + ");openAlerts()</script>";
                 
            }
            
            return RedirectToAction("home");

     }

      
      

        public ActionResult clients(string userId,string password)
        {
            encryptCompanyId = password;
            try
            {
                string decryptCompanyId = encryptDecrypt.Decrypt(password);    
                
            var user = entity.users.Where(a => a.userId.Equals(userId)).FirstOrDefault();
           
            if (ModelState.IsValid && decryptCompanyId.Equals(user.companyId.ToString()))
                return RedirectToAction("home");
                else
                return RedirectToAction("login");
            }
            catch (Exception e)
            {
                return RedirectToAction("login");
            }
            return RedirectToAction("login");
        }


        public ActionResult home()
        {           
                    return View();               
            }


        usersExcel summaryDefectModel = new usersExcel();
        public ActionResult dashboard()
        {
            try
            {
                var v = entity.userWorksapces.Where(a => a.userId.Equals("swan")).FirstOrDefault();
                v.msg = null; v.defectId = null; v.RunDate = null; v.fileName = null; v.alerts = null;
                entity.SaveChanges();
            }
            catch (Exception e) { }

            return RedirectToAction("about");
        }
        
     public ViewResult about()
        {
            /*  usersExcel user = new usersExcel();
                                           List<usersExcel> userList = new List<usersExcel>();
                                           while (reader.Read())
                                           {
                                               for (int row = 0; row <= 30; row++)                                                                                  
                                                   user.stringDay[row] = reader[row].ToString();   
                                            }

                                   for (int row = 0; row <= 30; row++)
                                       user.intDays[row] = Int32.Parse(user.stringDay[row]);

                                   userList.Add(user);  
                                   string jsondata = new JavaScriptSerializer().Serialize(userList);
                                   string NewString = jsondata.TrimEnd(']').TrimStart('[');
                                   string path = Server.MapPath("~/JSON/");
                                   System.IO.File.WriteAllText(path + "output.json", NewString);
                                    */


            return View();
        }
     

        [HttpPost]
        public DefectDetailsData[] LoadData(string api)
        { 
            string[]  rowData =Regex.Split(api, @"\D+");
            int defectId = Int32.Parse(rowData[1]);   
            int recordsTotal = entity.defectDetails.Where(a=>a.DefectId.Equals(defectId)).Count(),i=0;
           
            string queryString = "SELECT * FROM [dbo].[defectDetails]  where DefectId =" + defectId;
            DefectDetailsData[] localDdd = new DefectDetailsData[recordsTotal];

              

            //  return Json(new { localDdd } , JsonRequestBehavior.AllowGet);
            return localDdd;


        }
    }


}