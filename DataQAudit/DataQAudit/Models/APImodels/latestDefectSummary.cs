﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DataQAudit.Models
{
    public class latestDefectSummary
    {
       
        public string Date { get; set; }
       
        public decimal defectTable { get; set; }
        public decimal defectRows { get; set; }
        public decimal defectData { get; set; }
    }


    public class DefectSummaryData
    {

        public int DefectId { get; set; }
        public int CompanyId { get; set; }
        public string SourceTableName { get; set; }
        public string TargetTableName { get; set; }
        public string AuditResult { get; set; }
        public int DataAudited { get; set; }
        public int RowsAudited { get; set; }
        public int DataDefects { get; set; }
        public int RowsDefects { get; set; }
        public string DetailFileName { get; set; }
        public string Available { get; set; }
        public string AuditRun { get; set; }
        public string RunDate { get; set; }
        public virtual ICollection<defectDetail> defectDetails { get; set; }
        public virtual domain domain { get; set; }
        public int totalPages { get; set; }
    }
    public class DefectDetailsData
    {
        public int DefectId { get; set; }
        public string FwdRev { get; set; }
        public string UniqueId { get; set; }
        public string[] UniqueIdArray { get; set; }
        public string TargetColName { get; set; }
        public string TargetValue { get; set; }
        public string SourceValue { get; set; }
        public string Rows { get; set; }
        public string Columns { get; set; }
        public int totalPages { get; set; }
        public string[] uniqueColumnsAudited { get; set; }
        public int chunks { get; set; }
        public int totalChunks { get; set; }

    }

    public class tablesAuditedRunStats
    {

        public int id { get; set; }
        public int ReferenceId { get; set; }
        public string AuditCoverage { get; set; }
        public string RowChecked { get; set; }
        public string RowPassed { get; set; }
        public string MissinRows { get; set; }
        public string NonMissinsRowsError { get; set; }
        public string UniqueDataCellAudited { get; set; }
        public string UniqueDataCellPassed { get; set; }
        public string UniqueMissingDataCellAudited { get; set; }
        public string UniqueNonMissingDataCellError { get; set; }
        public string[] ColumnsAudited { get; set; }
        public string[] uniqueColumnsAudited { get; set; }
    }


}