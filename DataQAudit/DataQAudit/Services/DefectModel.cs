﻿using DataQAudit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace DataQAudit.Services
{
    public class DefectModel : Controller
    {
        swabzEntities entity = new swabzEntities();
        int pageSize = 5;
        decimal totalTables, defectedTables, DataAudited, DataDefects, RowsAudited, RowsDefects;
        SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        SqlCommand cmd;
       static int  MaxProcessSize = 100000;

        public latestDefectSummary[] GetLatestDefectTrend(string date, int DefectId, string SourceTableName, string TargetTableName)
        {
            DateTime dt_l = new DateTime();
            dt_l = getDate(date);
            latestDefectSummary[] localDFS = null;
            localDFS = calclateTrend(dt_l, DefectId, TargetTableName, SourceTableName, localDFS);
            return localDFS;
        }



        private latestDefectSummary[] calclateTrend(DateTime dt_l, int DefectId, string TargetTableName, string SourceTableName, latestDefectSummary[] localDFS)
        {
            int it = 0;
            try
            {
                var TableDataObj = entity.defectSummaries.Where(a => a.RunDate <= dt_l);

                var descDate = entity.defectSummaries.Where(a => a.RunDate <= dt_l).ToList().Select(r => r.RunDate.Date).Distinct().OrderByDescending(d => d.Date);

                if (TargetTableName != null && SourceTableName != null && DefectId != -1)
                {
                    TableDataObj = from cust in entity.defectSummaries where cust.TargetTableName.Equals(TargetTableName) && cust.SourceTableName.Equals(SourceTableName) && cust.RunDate <= dt_l select cust;
                  //  descDate = entity.defectSummaries.Where(a => a.RunDate <= dt_l && a.DefectId.Equals(DefectId)).ToList().Select(r => r.RunDate.Date).Distinct().OrderByDescending(d => d.Date);
                }
                else
                    TableDataObj = entity.defectSummaries;

                localDFS = new latestDefectSummary[descDate.Count()];
                foreach (var d in descDate)
                {
                    try
                    {
                        totalTables = TableDataObj.Where(a => a.RunDate.Equals(d)).Count();
                        defectedTables = TableDataObj.Where(a => a.AuditResult.Equals("Failed - Has Defects") && a.RunDate.Equals(d)).Count();
                        DataAudited = TableDataObj.Where(a => a.RunDate.Equals(d)).Sum(a => a.DataAudited);
                        DataDefects = TableDataObj.Where(a => a.RunDate.Equals(d)).Sum(a => a.DataDefects);
                        RowsAudited = TableDataObj.Where(a => a.RunDate.Equals(d)).Sum(a => a.RowsAudited);
                        RowsDefects = TableDataObj.Where(a => a.RunDate.Equals(d)).Sum(a => a.RowsDefects);
                    }
                    catch (Exception e) { continue; }
                    if (totalTables != 0)
                    {
                        localDFS[it] = new latestDefectSummary();
                        localDFS[it].defectTable = Math.Round(((defectedTables * 100) / totalTables), 2);
                        localDFS[it].defectRows = Math.Round(((RowsDefects * 100) / RowsAudited), 2);
                        localDFS[it].defectData = Math.Round(((DataDefects * 100) / DataAudited), 2);
                        localDFS[it].Date = d.Date.ToString("dd/MM/yyyy");
                        it++;
                    }
                    else
                    {
                        return localDFS;
                    }
                }
                return localDFS;
            }
            catch (Exception e)
            {
                return new latestDefectSummary[0];
            }
        }

        public DefectSummaryData[] getSummaryDataMethod(string audit, string rundate, int pageNo = 1)
        {

            DateTime dt_l = new DateTime();
            try
            {
                dt_l = getDate(rundate);

                var descDate = entity.defectSummaries.Where(a => a.RunDate <= dt_l).ToList().Select(r => r.RunDate.Date).Distinct().OrderBy(d => d.Date);
                dt_l = descDate.Max(a => a.Date);
                int i = 0, totalPage, totalRecord;
                pageSize = 5;
                //  var summaryData = (from data in entity.defectSummaries.Where(a=>a.AuditResult.Equals(audit)&&a.RunDate.Equals(dt_l)) orderby data.RunDate descending select data).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();
                var summaryData = (from data in entity.defectSummaries.Where(a => a.AuditResult.Equals(audit) && a.RunDate.Equals(dt_l)) orderby data.AuditResult select data).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();

                DefectSummaryData[] localSdb = new DefectSummaryData[summaryData.Count()];
                totalRecord = entity.defectSummaries.Where(a => a.AuditResult.Equals(audit) && a.RunDate.Equals(dt_l)).Count();
                totalPage = (totalRecord / pageSize) + ((totalRecord % pageSize) > 0 ? 1 : 0);


                foreach (var obj in summaryData)
                {
                    localSdb[i] = new DefectSummaryData();
                    localSdb[i].DefectId = obj.DefectId;
                    localSdb[i].RowsDefects = obj.RowsDefects;
                    localSdb[i].DataDefects = obj.DataDefects;
                    localSdb[i].SourceTableName = obj.SourceTableName;
                    localSdb[i].TargetTableName = obj.TargetTableName;
                    if (obj.DetailFileName.ToUpper().Contains("FORWARD"))
                    {
                        localSdb[i].DetailFileName = "Forward Compare";
                    }
                    else
                    {
                        localSdb[i].DetailFileName = "Reverse Compare";
                    }

                    localSdb[i].totalPages = totalPage;
                    localSdb[i].RunDate = obj.RunDate.ToString("dd/MM/yyyy");
                    i++;
                }
                return localSdb;
            }
            catch (Exception e) { }
            return new DefectSummaryData[0];
        }

      
        public DefectDetailsData[] getDetailsDataMethod2(int defectId, int chunkNo)
        {

            int recordsTotal = entity.defectDetails.Where(a => a.DefectId.Equals(defectId)).Count(), i = 0;
            string queryString = "SELECT * FROM [dbo].[defectDetails]  where DefectId =" + defectId;
            DefectDetailsData[] localDdd;
            
            try
            {
                using (conn)
                {
                    cmd = new SqlCommand(queryString, conn);
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    IDataRecord record;
                    int chunks;
                    int chunkno = chunkNo;
                    if (chunkno.Equals(0))
                    {
                        decimal chunk = Math.Round((decimal)recordsTotal / MaxProcessSize, 3);
                        string[] chunksParts = chunk.ToString().Split('.');
                        if (Int32.Parse(chunksParts[1]) > 0)
                            chunks = Int32.Parse(chunksParts[0]) + 1;
                        else
                            chunks = Int32.Parse(chunksParts[0]);
                        chunkno = chunks;
                        queryString = "UPDATE userWorksapce SET alerts=" + chunks + " WHERE id=17"; // using 'alerts' field as a "no. of chunks" 
                        cmd = new SqlCommand(queryString, conn);//string query 
                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        var userWorkSpce = entity.userWorksapces.Where(a => a.id.Equals(17)).FirstOrDefault();
                        chunks =Int32.Parse( userWorkSpce.alerts);
                    }

                    while (i++ < chunkNo * MaxProcessSize && reader.Read())
                    {                 }

                    if (chunks.Equals(chunkNo+1))
                    {
                        i--;
                        i = recordsTotal - i;
                        localDdd = new DefectDetailsData[i];
                        chunkNo++;
                        chunkno = 0;
                    }
                    else
                        if (recordsTotal < MaxProcessSize)
                        localDdd = new DefectDetailsData[recordsTotal];//recordsTotal
                    else
                        localDdd = new DefectDetailsData[MaxProcessSize];

                    i = 0;                 
                    while (reader.Read() && i != MaxProcessSize)
                        {
                            record = (IDataRecord)reader;
                            localDdd[i] = new DefectDetailsData();
                            localDdd[i].FwdRev = record[2].ToString();
                            localDdd[i].UniqueIdArray = record[3].ToString().Split(',');
                            localDdd[i].TargetColName = record[4].ToString();
                            localDdd[i].TargetValue = record[5].ToString();
                            localDdd[i].SourceValue = record[6].ToString();
                            localDdd[i].Rows = record[7].ToString();
                            localDdd[i++].Columns = record[8].ToString();                      
                    }                                   
                    localDdd[0].chunks = chunkNo;//re-using defectId int as a chunkNo.
                    localDdd[0].totalChunks = chunks;
                    reader.Close();
                    if (chunkNo.Equals(0)|| chunkno.Equals(0))//   chunks.Equals(chunkNo)
                    {
                      
                        var uniqueCol = entity.tablesAuditedColumns.Where(a => a.ReferenceId.Equals(defectId) && a.UniqueKey.Equals("TRUE")).ToList().Select(a => a.ColumnsAudited);
                        localDdd[0].uniqueColumnsAudited = new String[uniqueCol.Count()];
                        i = 0;
                        foreach (var a in uniqueCol)
                        {
                            localDdd[0].uniqueColumnsAudited[i++] = a.ToString();
                        }

                    }
                    
                }
                localDdd[0].totalPages = recordsTotal;//using 'totalPages' as totalRecords/rows in database to sync uploader bar
                return localDdd;
            }
            catch (Exception e)
            {
                return new DefectDetailsData[0];
            }

            //  return Json(new { localDdd } , JsonRequestBehavior.AllowGet);
          


        }
    
        public DefectDetailsData[] getDetailsDataMethod3(int defectId, int pageNo = 1)
        {
            int i = 0, totalPage, totalRecord;
            pageSize = 5;
            try
            {
                var detailsData = (from data in entity.defectDetails.Where(a => a.DefectId.Equals(defectId)) orderby data.DefectId select data).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();
                totalRecord = entity.defectDetails.Where(a => a.DefectId.Equals(defectId)).Count();
                DefectDetailsData[] localDdd = new DefectDetailsData[detailsData.Count()];
                var uniqueCol = entity.tablesAuditedColumns.Where(a => a.ReferenceId.Equals(defectId) && a.UniqueKey.Equals("TRUE")).ToList().Select(a => a.ColumnsAudited);


                totalPage = (totalRecord / pageSize) + ((totalRecord % pageSize) > 0 ? 1 : 0);
                foreach (var obj in detailsData)
                {
                    localDdd[i] = new DefectDetailsData();
                    localDdd[i].FwdRev = obj.Rev_Fwd;
                    localDdd[i].UniqueId = obj.UniqueId;
                    localDdd[i].UniqueIdArray = obj.UniqueId.ToString().Split(',');
                    localDdd[i].TargetColName = obj.TargetColName;
                    localDdd[i].TargetValue = obj.TargetValue;
                    localDdd[i].SourceValue = obj.SourceValue;
                    localDdd[i].Rows = obj.RowNum;
                    localDdd[i].Columns = obj.ColNum;
                    localDdd[i].DefectId = obj.DefectId;
                    localDdd[i].totalPages = totalPage;
                    i++;
                }
                i = 0;
                localDdd[0].uniqueColumnsAudited = new String[uniqueCol.Count()];
                foreach (var a in uniqueCol)
                {
                    localDdd[0].uniqueColumnsAudited[i++] = a.ToString();
                }
                return localDdd;
            }
            catch (Exception e)
            {
                return new DefectDetailsData[0];
            }

        }


        private DateTime getDate(string date)
        {
            var v = entity.userWorksapces.Where(a => a.userId.Equals("swan")).FirstOrDefault();
            DateTime dt_l = new DateTime();
            try
            {
                if (date != null)
                {
                    date = date.Replace("-", "/");
                    dt_l = DateTime.ParseExact(date, "dd/MM/yyyy", null);
                }
                else
                {
                    if (v.RunDate != null)
                        dt_l = (DateTime)v.RunDate;
                    else
                        dt_l = entity.defectSummaries.Max(i => i.RunDate);
                }

            }
            catch (Exception e) { TempData["msg"] = "<script>alert('in-valid date');</script>"; }

            try
            {
                v.RunDate = dt_l;
                entity.SaveChanges();
            }
            catch (Exception e) { }
            return dt_l;
        }



        public tablesAuditedRunStats getDetailsStats(int defectId)
        {
            tablesAuditedRunStats obj = new tablesAuditedRunStats();
            var stats = entity.tablesAuditedRunStats.Where(a => a.ReferenceId.Equals(defectId)).FirstOrDefault();
            var audit = entity.tablesAuditedColumns.Where(a => a.ReferenceId.Equals(defectId)).ToList().Select(a => a.ColumnsAudited);
            //    var uniqueCol= entity.tablesAuditedColumns.Where(a => a.ReferenceId.Equals(defectId)&&a.UniqueKey.Equals("TRUE")).ToList().Select(a => a.ColumnsAudited);
            int i = 0;
            try
            {
                obj.AuditCoverage = stats.AuditCoverage;
                obj.RowChecked = stats.RowChecked;
                obj.RowPassed = stats.RowPassed;
                obj.MissinRows = stats.MissinRows;
                obj.NonMissinsRowsError = stats.NonMissinsRowsError;
                obj.UniqueDataCellAudited = stats.UniqueDataCellAudited;
                obj.UniqueDataCellPassed = stats.UniqueDataCellPassed;
                obj.UniqueMissingDataCellAudited = stats.UniqueMissingDataCellAudited;
                obj.UniqueNonMissingDataCellError = stats.UniqueNonMissingDataCellError;
                obj.ColumnsAudited = new String[audit.Count()];
                //  obj.uniqueColumnsAudited = new String[uniqueCol.Count()];
            }
            catch (Exception e)
            {
                return null;
            }
            /*   foreach (var a in uniqueCol)
               {
                   obj.uniqueColumnsAudited[i++] = a.ToString();
               }*/
            i = 0;
            foreach (var a in audit)
            {
                obj.ColumnsAudited[i++] = a.ToString();
            }
            return obj;
        }


   
    }
}