//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataQAudit
{
    using System;
    using System.Collections.Generic;
    
    public partial class defectSummary
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public defectSummary()
        {
            this.defectDetails = new HashSet<defectDetail>();
            this.tablesAuditedColumns = new HashSet<tablesAuditedColumn>();
            this.tablesAuditedRunStats = new HashSet<tablesAuditedRunStat>();
        }
    
        public int DefectId { get; set; }
        public int CompanyId { get; set; }
        public string SourceTableName { get; set; }
        public string TargetTableName { get; set; }
        public string AuditResult { get; set; }
        public int DataAudited { get; set; }
        public int RowsAudited { get; set; }
        public int DataDefects { get; set; }
        public int RowsDefects { get; set; }
        public string DetailFileName { get; set; }
        public string Available { get; set; }
        public string AuditRun { get; set; }
        public DateTime RunDate { get; set; }
    
        public virtual domain domain { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<defectDetail> defectDetails { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tablesAuditedColumn> tablesAuditedColumns { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tablesAuditedRunStat> tablesAuditedRunStats { get; set; }
    }
}
